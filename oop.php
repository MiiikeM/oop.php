<?php

$instance = new Mestak;
// definije class Mestak
class Mestak {
    //definuje typ funkce
    public $JDM1 = 10; 
    public $JDM2 = 3;
    public $JDM3 = 50;
    public $JDM4 = 11;
    public $JDM5 = 29;
 //definuje chráněné funkce
    protected $JDM6 = 26;
    protected $JDM7 = 31; 
    protected $JDM8 = 36;
//definuje privátní funkce
    private $JDM9 = 189;
    private $JDM10 = 65;
//definuje veřejné funkce
    public function JDMJDM1 () {
       return $this-> JDM1;
    }
//definuje veřejné funkce
    public function JDMJDM2 () {
       return $this-> JDM2;
    }
//definuje veřejné funkce
    public function JDMJDM3 () {
       return $this-> JDM3;
    }
//definuje veřejné funkce    
    public function JDMJDM4 () {
       return $this-> JDM4;
    }
//definuje veřejné funkce    
    public function JDMJDM5 () {
       return $this-> JDM5;
    }
//definuje privátní funkce    
    private function setJDM9 () {
       return $this-> JDM9; 
    }
//definuje privátní funkce    
    private function setJDM10 () {
        return $this-> JDM10;
    }    
    public function _construct () {

    }
} 
//vypíše danné hodnoty
var_dump ($instance);
//vypíše hodnotu getJDM1
$JDM = new Mestak();
echo '<br>';
$JDM1-> getJDM1('10');
echo $JDM1-> getJDM1();
echo '<br>';
//vypíše hodnotu getJDM2
$JDM2-> getJDM2('3');
echo $JDM2-> getJDM2();
echo '<br>';
$JDM3-> getJDM3('50');
//vypíše hodnotu getJDM3
echo $JDM3-> getJDM3();
echo '<br>';
//vypíše hodnotu getJDM4
$JDM4-> getJDM4('11');
echo $JDM4-> getJDM4();
echo '<br>';
//vypíše hodnotu getJDM5
$JDM5-> getJDM5('29');
echo $JDM5-> getJDM5();
echo '<br>';

?> 